package com.example.notepad;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashSet;
import java.util.Objects;


public class SignUp extends Fragment {

    View view;
    EditText nameNew, emailNew, passwordNew;
    MainActivity mainActivity;
    String newName, newEmail, newPass;
    DbHelper DB;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        nameNew = view.findViewById(R.id.name);
        emailNew = view.findViewById(R.id.newEmail);
        passwordNew = view.findViewById(R.id.password);
        DB = new DbHelper(getContext());
        mainActivity = (MainActivity) getActivity();
        mainActivity.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newName = nameNew.getText().toString();
                newEmail = emailNew.getText().toString();
                newPass = passwordNew.getText().toString();
                if (newEmail.isEmpty()||newPass.isEmpty()){
                    Toast.makeText(getContext(), "Fields can not be empty!", Toast.LENGTH_SHORT).show();
                }else{
                    Boolean checkUser = DB.checkUser(newEmail);
                    if (checkUser==false){
                        Boolean insert = DB.insertData(newEmail, newPass);
                        if (insert==true){
                            Toast.makeText(getContext(), "Registration Sucess!!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getContext(), HomeScreen.class);
                            startActivity(intent);
                        }else {
                            Toast.makeText(getContext(), "Registration Failed!", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getContext(), "email already exists", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        return view;
    }
}