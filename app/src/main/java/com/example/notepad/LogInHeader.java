package com.example.notepad;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class LogInHeader extends Fragment {

    View view;
    Button signUp;
    MainActivity mainActivity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_log_in_header, container, false);
        signUp = view.findViewById(R.id.signUp);
        mainActivity = (MainActivity) getActivity();
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignUpHeader signUpHeader = new SignUpHeader();
                getFragmentManager().beginTransaction().replace(R.id.header, signUpHeader).addToBackStack(null).commit();
                SignUp signUp = new SignUp();
                getFragmentManager().beginTransaction().replace(R.id.fragmentFrame, signUp).addToBackStack(null).commit();
                mainActivity.button.setText("Sign Up");
            }
        });
        return view;
    }
}