package com.example.notepad;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.util.Objects;


public class SignIn extends Fragment {
    int RC_SIGN_IN = 0;
    GoogleSignInClient mGoogleSignInClient;
    SignInButton button;
    String personPass ;
    String personEmail ;
    TextView register;
    MainActivity mainActivity;
    View view;
    EditText emailId, password1;
    DbHelper DB;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        button = view.findViewById(R.id.sign_in_button);
        register = view.findViewById(R.id.registerNow);
        mainActivity = (MainActivity) getActivity();
        emailId= view.findViewById(R.id.emailId);
        DB = new DbHelper(getContext());
        password1 = view.findViewById(R.id.password);
        mainActivity.button.setText("Sign In");
        mainActivity.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = emailId.getText().toString();
                String passwordCheck = password1.getText().toString();
                if (email.isEmpty()||passwordCheck.isEmpty()){
                    Toast.makeText(mainActivity, "Fields can not be empty!", Toast.LENGTH_SHORT).show();
                }else{
                    Boolean checkUserData = DB.checkData(email, passwordCheck);
                    if (checkUserData==true){
                        Intent intent = new Intent(getContext(), HomeScreen.class);
                        Toast.makeText(mainActivity, "Welcome Back!", Toast.LENGTH_SHORT).show();
                        Log.i("checkfromSignIn", "success!!");
                        startActivity(intent);
                    }else{
                        Toast.makeText(mainActivity, "Incorrect Email or PassWord", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignUpHeader signUpHeader = new SignUpHeader();
                getFragmentManager().beginTransaction().replace(R.id.header, signUpHeader).addToBackStack(null).commit();
                SignUp signUp = new SignUp();
                getFragmentManager().beginTransaction().replace(R.id.fragmentFrame, signUp).addToBackStack(null).commit();
                mainActivity.button.setText("Sign Up");
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.sign_in_button:
                        signIn();
                        break;
                    // ...
                }
            }
        });
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(Objects.requireNonNull(getActivity()), gso);
        return view;
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(Objects.requireNonNull(getContext()));
            if (acct != null) {
                String name, email;
                name= acct.getDisplayName();
                email = acct.getEmail();
                Toast.makeText(getContext(), "Welcome "+name +"!!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getContext(), HomeScreen.class);

                startActivity(intent);

            }
        } catch (ApiException e) {
            Log.w("error", "signInResult:failed code=" + e.getStatusCode());
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(Objects.requireNonNull(getContext()));
    }
}