package com.example.notepad;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class SignUpHeader extends Fragment {
    View view;
    Button signIn;
    MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sign_up_header, container, false);
        signIn = view.findViewById(R.id.signIn);
        mainActivity = (MainActivity) getActivity();
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogInHeader signUpHeader = new LogInHeader();
                getFragmentManager().beginTransaction().replace(R.id.header, signUpHeader).addToBackStack(null).commit();
                SignIn signUp = new SignIn();
                getFragmentManager().beginTransaction().replace(R.id.fragmentFrame, signUp).addToBackStack(null).commit();
            }
        });

        return view;

    }
}